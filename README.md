# nwDAQ packages feed

## Description

This is an OpenWrt package feed containing nwDAQ software and the da_ui interface.

## Usage

To use these packages, add the following line to the feeds.conf
in the OpenWrt buildroot:

```
src-git nwdaq https://gitlab.com/nwdaq/openwrt-packages.git
```

